# TestArchitecture

TestArchitecture is a project made to develop a good architecture and to use it to continue learning and improving in the future 


## Architecture

The architecture used is MVVM and it has the following main components.
 - Views: (Activities and fragments) who observe the changes from live data variables in view model and paint the screen. And also receive user input.
 - ViewModels: It is where are all the controls for interacting with the View. It contains some variables to be observed by Views. 
 - UseCases: The UseCase are classes to do a determinate task. It connects with repositories.
 - Repository: They handle the access to the different sources of information. In this app we have two different data sources: Local and remote.
 - DataSource: They get the data, either from a service to a remote, or from the local data base.


## Libraries used

- [ ] [Hilt](https://developer.android.com/training/dependency-injection/hilt-android?hl=es-419) We use Hilt for dependency injection in the app.
- [ ] [Navigation](https://developer.android.com/guide/navigation) Jetpack library to handle the navigation through the app.
- [ ] [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) Stores and handle the data related to a UI in an improved way.
- [ ] [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) Jetpack library to generate containers for data to be observed. It also handles the data through life cycle of views.
- [ ] [Glide](https://github.com/bumptech/glide) Glide is a library to handle easily painting images from URLs.
- [ ] [Retrofit](https://github.com/square/retrofit) Retrofit is a library to handle service petitions.
- [ ] [Room](https://developer.android.com/reference/androidx/room/package-summary) Room is a Database Object Mapping library that makes it easy to access database on Android applications.
- [ ] [Dots indicator](https://github.com/rorpheeyah/dotsindicator) Library that makes easier to represent dots for a viewpager.
- [ ] [KotlinCoroutines](https://kotlinlang.org/docs/coroutines-overview.html) For managing background threads with simplified code and reducing needs for callbacks.


## ToDo list. Things that I have not had time to develop yet.

- Better handle of service errors through the app. I left only a generic error.
- Delete the cache after some time. I created a variable for storing the time but it is not being used yet.
- Improve the UI, the design could be better.
- Make some unit testing. In the future we could add unit testing to the different pieces of the app.
