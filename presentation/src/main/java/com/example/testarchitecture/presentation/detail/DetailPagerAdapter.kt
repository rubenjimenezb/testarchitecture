package com.example.testarchitecture.presentation.detail

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.testarchitecture.domain.model.TvShow

class DetailPagerAdapter(fragment: Fragment, val tvShowList: List<TvShow>) : FragmentStateAdapter(fragment) {

    // Returns total number of pages
    override fun getItemCount(): Int {
        return tvShowList.size
    }

    // Returns the fragment to display for that page
    override fun createFragment(position: Int): Fragment {
        return DetailItemFragment.newInstance(tvShowList[position])
    }
}