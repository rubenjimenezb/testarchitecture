package com.example.testarchitecture.presentation.navigation

import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import com.example.testarchitecture.R
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDialogNavigator @Inject constructor() {

    /**
     * Current dialog
     */
    private var currentDialog: Dialog? = null

    /**
     * Open progress dialog
     */
    fun showProgressDialog(context: Context) {
        currentDialog?.cancel()

        // Show alert in main thread
        val mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post { //inflate view
            val flLoading = LayoutInflater.from(context).inflate(R.layout.lay_full_screen_view_loader, null) as FrameLayout

            // Set up dialog
            val dialog = Dialog(context, R.style.LoadingTheme)
            dialog.setContentView(flLoading)
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)

            dialog.window?.attributes?.windowAnimations = R.style.FadeInDialogAnimation //style id

            // Show dialog
            currentDialog = dialog
            currentDialog!!.show()
        }
    }

    /**
     * Hide current dialog from screen
     */
    fun hideCurrentProgressDialog() {
        try {
            if (currentDialog != null && currentDialog!!.isShowing) {
                currentDialog!!.dismiss()
            }
            currentDialog = null
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            currentDialog = null
        }
    }

    fun showError(
        context: Context,
        onRetry: (() -> Unit)? = null,
    ) {
        currentDialog = AlertDialog.Builder(context)
            .setTitle(R.string.alert_title)
            .setMessage(R.string.alert_description)
            .setPositiveButton(R.string.alert_retry){ dialog, _ ->
                dialog.cancel()
                onRetry?.invoke()
            }
            .setNegativeButton(R.string.alert_cancel){ dialog, _ ->
                dialog.cancel()
            }
            .setCancelable(true)
            .show()
    }
}