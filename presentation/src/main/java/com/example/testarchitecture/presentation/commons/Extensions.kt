package com.example.testarchitecture.presentation.commons

import android.view.View

fun View.hide(hide: Boolean = true) {
    if (hide) this.visibility = View.GONE else show()
}

fun View.show(show: Boolean = true) {
    if (show) this.visibility = View.VISIBLE else hide()
}