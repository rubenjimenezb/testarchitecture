package com.example.testarchitecture.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel(
    private val coroutineExceptionHandler: CoroutineExceptionHandler = CoroutineExceptionHandler { _, exception ->
        Log.e(
            "BaseViewModel",
            "CoroutineExceptionHandler handled crash $exception \n ${exception.cause?.message}"
        )
    },
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    private var _scope =
        CoroutineScope(dispatcher + SupervisorJob() + coroutineExceptionHandler)
    protected val scope
        get() = _scope

    private fun reinitScope() {
        if (!isJobActive()) {
            _scope = CoroutineScope(dispatcher + SupervisorJob() + coroutineExceptionHandler)
        }
    }

    private fun isJobActive() = _scope.isActive

    protected fun launch(dispatcher: CoroutineContext = this.dispatcher, block: suspend () -> Unit): Job {
        reinitScope()
        return _scope.launch(context = dispatcher) {
            block()
        }
    }

}