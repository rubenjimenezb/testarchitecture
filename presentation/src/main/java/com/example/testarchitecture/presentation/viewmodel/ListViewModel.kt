package com.example.testarchitecture.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testarchitecture.domain.model.TvShow
import com.example.testarchitecture.domain.usecase.GetNextPageTopRatedTvShows
import com.example.testarchitecture.domain.usecase.GetTopRatedTvShows
import com.example.testarchitecture.presentation.di.VMDispatcher
import com.example.testarchitecture.presentation.model.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject internal constructor(
    private val getTopRatedTvShows: GetTopRatedTvShows,
    private val getNextPageTopRatedTvShows: GetNextPageTopRatedTvShows,
    @VMDispatcher dispatcher: CoroutineDispatcher,
) : BaseViewModel(dispatcher = dispatcher) {

    private val _tvShowResponse: MutableLiveData<NetworkResult<List<TvShow>>> = MutableLiveData()
    val tvShowResponse: LiveData<NetworkResult<List<TvShow>>> get() = _tvShowResponse

    init {
        getTvShowList()
    }

    fun getTvShowList() {
        launch {

            _tvShowResponse.postValue(NetworkResult.Loading())
            getTopRatedTvShows().fold(
                ifLeft = { failure ->
                    Log.d(
                        "Error in ListViewModel",
                        " error when getting top rated tv shows $failure"
                    )
                    _tvShowResponse.postValue(NetworkResult.Error(message = failure.toString()))
                },
                ifRight = { tvShowList ->
                    _tvShowResponse.postValue(NetworkResult.Success(tvShowList))
                }
            )
        }
    }

    fun getNextPage() {
        launch {

            _tvShowResponse.postValue(NetworkResult.Loading())
            getNextPageTopRatedTvShows().fold(
                ifLeft = { failure ->
                    Log.d(
                        "Error in ListViewModel",
                        " error when getting next page top rated tv shows $failure"
                    )
                    _tvShowResponse.postValue(NetworkResult.Error(message = failure.toString()))
                },
                ifRight = { tvShowList ->
                    //TODO:Rj check if the list is equal to the previous one we had,
                    // to disable the pagination.
                    _tvShowResponse.postValue(NetworkResult.Success(tvShowList))
                }
            )
        }
    }
}