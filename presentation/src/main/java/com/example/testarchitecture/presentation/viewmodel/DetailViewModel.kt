package com.example.testarchitecture.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testarchitecture.domain.model.TvShow
import com.example.testarchitecture.domain.usecase.GetSimilarTvShows
import com.example.testarchitecture.domain.usecase.GetTvShowDetail
import com.example.testarchitecture.presentation.di.VMDispatcher
import com.example.testarchitecture.presentation.model.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject internal constructor(
    private val getTvShowDetail: GetTvShowDetail,
    private val getSimilarTvShows: GetSimilarTvShows,
    @VMDispatcher dispatcher: CoroutineDispatcher,
) : BaseViewModel(dispatcher = dispatcher) {

    private val _tvShowDetail: MutableLiveData<NetworkResult<TvShow>> = MutableLiveData()
    val tvShowDetail: LiveData<NetworkResult<TvShow>> get() = _tvShowDetail

    fun getTvShowDetail(id: Int) {
        launch {
            getTvShowDetail(params = GetTvShowDetail.Params(id = id)).fold(
                ifLeft = { failure ->
                    Log.d(
                        "Error in DetailViewModel",
                        " error when getting top rated detail $failure"
                    )
                    _tvShowDetail.postValue(NetworkResult.Error(message = failure.toString()))
                },
                ifRight = { tvShowDetail ->
                    tvShowDetail?.let {
                        if (tvShowDetail.similarList.isNullOrEmpty()) {
                            getSimilarTvShowList(id)
                        } else {
                            _tvShowDetail.postValue(NetworkResult.Success(tvShowDetail))
                        }
                    } ?: _tvShowDetail.postValue(NetworkResult.Error(message = "Detail not found"))
                }
            )
        }
    }

    private fun getSimilarTvShowList(id: Int) {
        launch {
            _tvShowDetail.postValue(NetworkResult.Loading())
            getSimilarTvShows(params = GetSimilarTvShows.Params(id = id)).fold(
                ifLeft = { failure ->
                    Log.d(
                        "Error in DetailViewModel",
                        " error when getting similar Tv shows $failure"
                    )
                    //Not showing anything, this is done only in background
                },
                ifRight = { tvShowDetail ->
                    tvShowDetail?.let {
                        _tvShowDetail.postValue(NetworkResult.Success(tvShowDetail))
                    }
                }
            )
        }
    }
}