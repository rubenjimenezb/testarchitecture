package com.example.testarchitecture.presentation.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.testarchitecture.R
import com.example.testarchitecture.databinding.FragmentDetailItemBinding
import com.example.testarchitecture.domain.model.TvShow

private const val ARG_PARAM_TV_SHOW_DETAIL = "ARG_PARAM_TV_SHOW_DETAIL"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailItemFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailItemFragment : Fragment() {

    private var _binding: FragmentDetailItemBinding? = null
    private val binding: FragmentDetailItemBinding
        get() = _binding!!

    private var tvShowDetail: TvShow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            tvShowDetail = it.getSerializable(ARG_PARAM_TV_SHOW_DETAIL) as TvShow
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = FragmentDetailItemBinding.inflate(inflater, container, false)
            .also { _binding = it }.root

        binding.apply {

             tvShowDetail?.imageUrl?.let { url ->
                 Glide.with(root.context)
                     .load(url)
                     .transition(DrawableTransitionOptions.withCrossFade())
                     .into(appBarImage)
             }

             collapsingToolbar.title = tvShowDetail?.title ?: ""

             tvTvShowDescription.text = tvShowDetail?.overview ?: ""
             tvTvShowVoteAverage.text = root.context.getString(R.string.tv_show_vote_average, tvShowDetail?.voteAverage ?: "")
        }

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param tvShowDetail Parameter 1 TvShow.
         * @return A new instance of fragment DetailItemFragment.
         */
        @JvmStatic
        fun newInstance(tvShowDetail: TvShow) =
            DetailItemFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM_TV_SHOW_DETAIL, tvShowDetail)
                }
            }
    }
}