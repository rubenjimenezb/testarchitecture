package com.example.testarchitecture.presentation.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.testarchitecture.databinding.FragmentDetailPagerBinding
import com.example.testarchitecture.domain.model.TvShow
import com.example.testarchitecture.presentation.commons.hide
import com.example.testarchitecture.presentation.commons.show
import com.example.testarchitecture.presentation.model.NetworkResult
import com.example.testarchitecture.presentation.navigation.AppDialogNavigator
import com.example.testarchitecture.presentation.navigation.observe
import com.example.testarchitecture.presentation.viewmodel.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 * Use the [DetailPagerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class DetailPagerFragment : Fragment() {

    private var _binding: FragmentDetailPagerBinding? = null
    private val binding: FragmentDetailPagerBinding
        get() = _binding!!

    private val args: DetailPagerFragmentArgs by navArgs()

    private val viewModel: DetailViewModel by viewModels()

    @Inject
    lateinit var appDialogNavigator: AppDialogNavigator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentDetailPagerBinding.inflate(inflater, container, false)
    .also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initViewModel()
    }
    private fun initView() {
        getTvShowDetail()
    }

    private fun initViewModel() {
        observe(viewModel.tvShowDetail) { detailResult ->
            binding.apply {
                when (detailResult) {
                    is NetworkResult.Success -> {
                        appDialogNavigator.hideCurrentProgressDialog()
                        bindAdapterWithTvShowsDetail(detailResult.data)
                    }
                    is NetworkResult.Error -> {
                        appDialogNavigator.hideCurrentProgressDialog()
                        appDialogNavigator.showError(
                            context = requireContext(),
                            onRetry = {
                                getTvShowDetail()
                            }
                        )
                    }
                    is NetworkResult.Loading -> {
                        appDialogNavigator.showProgressDialog(context = requireContext())
                    }
                }
            }
        }
    }

    private fun bindAdapterWithTvShowsDetail(tvShowDetail: TvShow?) {
        tvShowDetail?.let { tvShow ->
            val tvShowDetailList = mutableListOf(tvShow)
            if(!tvShow.similarList.isNullOrEmpty()){
                tvShowDetailList.addAll(tvShow.similarList)
            }

            val adapter = DetailPagerAdapter(fragment = this, tvShowList = tvShowDetailList.toList() )

            binding.apply {
                vpTvShows.adapter = adapter

                if(tvShowDetailList.size <= 1){
                    springDotsIndicator.hide()
                }else{
                    springDotsIndicator.show()
                    springDotsIndicator.setViewPager2(vpTvShows)
                }
            }
        }
    }

    private fun getTvShowDetail(){
        viewModel.getTvShowDetail(args.idDetail)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment DetailFragment.
         */
        @JvmStatic
        fun newInstance() =
            DetailPagerFragment()
    }
}