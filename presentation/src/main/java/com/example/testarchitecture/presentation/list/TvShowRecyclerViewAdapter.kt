package com.example.testarchitecture.presentation.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.testarchitecture.R
import com.example.testarchitecture.databinding.ViewTvShowBinding
import com.example.testarchitecture.domain.model.TvShow

class TvShowRecyclerViewAdapter(
    private var showList: List<TvShow>,
    private val onShowSelected: (Int) -> Unit = {},
) : RecyclerView.Adapter<TvShowRecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ViewTvShowBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(show: TvShow) {
            binding.apply {
                tvTvShowName.text = show.title
                tvTvShowVoteAverage.text = root.context.getString(R.string.tv_show_vote_average, show.voteAverage)

                show.imageUrl.let { url ->
                    Glide.with(root.context)
                        .load(url)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(ivTvShowImage)
                }

                itemView.setOnClickListener {
                    onShowSelected(show.id)
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TvShowRecyclerViewAdapter.ViewHolder = ViewHolder(
        binding = ViewTvShowBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(
        holder: TvShowRecyclerViewAdapter.ViewHolder,
        position: Int
    ) {
        showList[position].let { show ->
            holder.bind(show)
        }
    }

    override fun getItemCount(): Int = showList.size

    fun refreshList(list: List<TvShow>){
        showList = list
        notifyDataSetChanged()
    }
}