package com.example.testarchitecture.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testarchitecture.databinding.FragmentListBinding
import com.example.testarchitecture.presentation.model.NetworkResult
import com.example.testarchitecture.presentation.navigation.AppDialogNavigator
import com.example.testarchitecture.presentation.navigation.observe
import com.example.testarchitecture.presentation.viewmodel.ListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Use the [ListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding
        get() = _binding!!

    private val viewModel: ListViewModel by viewModels()

    @Inject
    lateinit var appDialogNavigator: AppDialogNavigator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentListBinding.inflate(inflater, container, false)
        .also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        initListener()
        initViewModel()
    }

    private fun initView() {
        binding.apply {

            rvTvShow.apply {
                layoutManager = LinearLayoutManager(root.context)
                adapter = TvShowRecyclerViewAdapter(
                    showList = emptyList(),
                    onShowSelected = { showId ->
                        val action =
                            ListFragmentDirections.actionListFragmentToDetailFragment(showId)
                        root.findNavController().navigate(action)
                    }
                )
            }

        }
    }

    private fun initListener() {
        binding.apply {
            rvTvShow.addOnScrollListener(object : RecyclerView.OnScrollListener(){
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if(!recyclerView.canScrollVertically(1)) {
                        viewModel.getNextPage()
                    }
                }
            })
        }
    }

    private fun initViewModel() {
        observe(viewModel.tvShowResponse) { result ->
            binding.apply {
                when (result) {
                    is NetworkResult.Success -> {
                        appDialogNavigator.hideCurrentProgressDialog()
                        (rvTvShow.adapter as? TvShowRecyclerViewAdapter?)?.refreshList(
                            (result as NetworkResult.Success).data ?: emptyList()
                        )
                    }
                    is NetworkResult.Error -> {
                        appDialogNavigator.hideCurrentProgressDialog()
                        appDialogNavigator.showError(
                            context = requireContext(),
                            onRetry = {
                                viewModel.getTvShowList()
                            }
                        )
                    }
                    is NetworkResult.Loading -> {
                        appDialogNavigator.showProgressDialog(context = requireContext())
                    }
                }
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ListFragment.
         */
        @JvmStatic
        fun newInstance() =
            ListFragment()
    }
}