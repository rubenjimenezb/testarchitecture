package com.example.testarchitecture.domain.error

sealed class Failure(val data: Any? = null) {
    object ServerError : Failure()

    /** * Extend this class for feature specific failures.*/
    abstract class FeatureFailure : Failure()
}
