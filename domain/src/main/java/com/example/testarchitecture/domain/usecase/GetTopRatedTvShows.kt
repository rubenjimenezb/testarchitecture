package com.example.testarchitecture.domain.usecase

import com.example.testarchitecture.domain.error.Failure
import com.example.testarchitecture.domain.functional.Either
import com.example.testarchitecture.domain.model.TvShow
import com.example.testarchitecture.domain.repository.TvShowsRepository
import javax.inject.Inject

class GetTopRatedTvShows @Inject constructor(
    private val repository: TvShowsRepository
) : UseCaseSuspend<Either<Failure, List<TvShow>>, Unit>() {

    override suspend fun run(params: Unit?): Either<Failure, List<TvShow>> {
        return repository.getTopRatedTvShows()
    }
}