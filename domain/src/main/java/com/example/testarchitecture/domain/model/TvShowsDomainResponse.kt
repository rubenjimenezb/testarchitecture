package com.example.testarchitecture.domain.model

data class TvShowsDomainResponse(
    val page: Int,
    val totalPages: Int,
    val results: List<TvShow>,
)