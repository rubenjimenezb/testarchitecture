package com.example.testarchitecture.domain.model

import java.io.Serializable

data class TvShow(
    val id: Int,
    val title: String,
    val imageUrl: String,
    val overview: String,
    val voteAverage: Float,
    var similarList: List<TvShow> = emptyList()
) : Serializable
