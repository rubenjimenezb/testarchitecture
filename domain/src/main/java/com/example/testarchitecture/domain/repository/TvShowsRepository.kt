package com.example.testarchitecture.domain.repository

import com.example.testarchitecture.domain.error.Failure
import com.example.testarchitecture.domain.functional.Either
import com.example.testarchitecture.domain.model.TvShow

interface TvShowsRepository {
    suspend fun getTopRatedTvShows(): Either<Failure, List<TvShow>>
    suspend fun getNextPageOfTopRatedTvShows(): Either<Failure, List<TvShow>>
    suspend fun getTvShowDetail(id: Int): Either<Failure, TvShow?>
    suspend fun getSimilarTvShows(id: Int): Either<Failure, TvShow?>


}