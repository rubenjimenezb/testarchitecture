package com.example.testarchitecture.domain.usecase

import com.example.testarchitecture.domain.error.Failure
import com.example.testarchitecture.domain.functional.Either
import com.example.testarchitecture.domain.model.TvShow
import com.example.testarchitecture.domain.repository.TvShowsRepository
import javax.inject.Inject

class GetTvShowDetail @Inject constructor(
    private val repository: TvShowsRepository
) : UseCaseSuspend<Either<Failure, TvShow?>, GetTvShowDetail.Params>() {

    override suspend fun run(params: Params?): Either<Failure, TvShow?> {
        return repository.getTvShowDetail(id = params?.id ?: 0)
    }

    data class Params(val id: Int)

}