package com.example.testarchitecture.domain.model

data class Session (
    val idSession: Int = 0,
    var topRatedTvShowLastPageAsked: Int,
    var topRatedTvShowTotalPages: Int,
    val dateCachePetition: String,
)
