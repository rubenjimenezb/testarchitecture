package com.example.testarchitecture.data.service

import com.example.testarchitecture.data.model.TvShowsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieDatabaseService {

    @POST(MOVIE_DATABASE_TOP_RATED_ENDPOINT)
    fun getTopRatedTvShows(
        @Query(MOVIE_DATABASE_API_KEY_VALUE) apyKey: String = MOVIE_DATABASE_API_KEY_KEY,
        @Query(MOVIE_DATABASE_PAGE_VALUE) page: Int? = null,
    ): Call<TvShowsResponse>

    @GET(MOVIE_DATABASE_SIMILAR_SHOWS_ENDPOINT)
    fun getSimilarTvShows(
        @Path("tv_id") id: Int,
        @Query(MOVIE_DATABASE_API_KEY_VALUE) apyKey: String = MOVIE_DATABASE_API_KEY_KEY,
    ): Call<TvShowsResponse>
}