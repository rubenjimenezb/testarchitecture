package com.example.testarchitecture.data.model

open class BaseNetworkApiResponse {
    open fun isSuccess(): Boolean = true
}
