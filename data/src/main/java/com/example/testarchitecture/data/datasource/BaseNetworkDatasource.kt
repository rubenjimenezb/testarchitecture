package com.example.testarchitecture.data.datasource

import com.example.testarchitecture.domain.error.Failure
import com.example.testarchitecture.domain.functional.Either
import retrofit2.Call

open class BaseNetworkDatasource {
    fun <T, R> request(
        call: Call<T>,
        transform: (T) -> R,
        default: T
    ): Either<Failure, R> {
        return try {
            val response = call.execute()
            when (response.isSuccessful) {
                true -> Either.Right(transform((response.body() ?: default)))
                false -> Either.Left(Failure.ServerError)
            }
        } catch (exception: Throwable) {
            Either.Left(Failure.ServerError)
        }
    }
}