package com.example.testarchitecture.data.di

import android.content.Context
import com.example.testarchitecture.data.database.AppDatabase
import com.example.testarchitecture.data.database.dao.SessionDao
import com.example.testarchitecture.data.database.dao.TvShowDao
import com.example.testarchitecture.data.database.datasource.LocalSessionDatasource
import com.example.testarchitecture.data.database.datasource.LocalTvShowDatasource
import com.example.testarchitecture.data.datasource.TvShowsDatasource
import com.example.testarchitecture.data.handler.DateHandler
import com.example.testarchitecture.data.repository.TvShowsDataRepository
import com.example.testarchitecture.data.service.MOVIE_DATABASE_BASE_URL
import com.example.testarchitecture.data.service.MOVIE_DATABASE_DEFAULT_TIMEOUT
import com.example.testarchitecture.data.service.MovieDatabaseService
import com.example.testarchitecture.domain.repository.TvShowsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun provideTopRatedTvShowRepository(
        tvShowsDatasource: TvShowsDatasource,
        localTvShowDatasource: LocalTvShowDatasource,
        localSessionDatasource: LocalSessionDatasource,
        dateHandler: DateHandler
        ): TvShowsRepository {
        return TvShowsDataRepository(
            tvShowsDatasource = tvShowsDatasource,
            localTvShowDatasource = localTvShowDatasource,
            localSessionDatasource = localSessionDatasource,
            dateHandler = dateHandler,
        )
    }

    @Provides
    @Singleton
    fun provideMovieDatabaseService(
        @MovieDatabaseUrl url: String,
        @HttpClient client: OkHttpClient
    ): MovieDatabaseService {

        val retrofit = Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(url)
            .build()

        return retrofit.create(MovieDatabaseService::class.java)
    }

    @Provides
    @Singleton
    @MovieDatabaseUrl
    fun provideCexRestUrl(): String =
        MOVIE_DATABASE_BASE_URL

    @Provides
    @Singleton
    @HttpClient
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(MOVIE_DATABASE_DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .build()

    @Provides
    @Singleton
    fun provideTvShowDao(
        appDatabase: AppDatabase
    ): TvShowDao {
        return appDatabase.tvShowDao()
    }

    @Provides
    @Singleton
    fun provideSessionDao(
        appDatabase: AppDatabase
    ): SessionDao {
        return appDatabase.sessionDao()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(
        @ApplicationContext context: Context,
        @DatabaseName databaseName: String
    ): AppDatabase {
        return AppDatabase.getDatabase(context, databaseName, false)
    }

    @Provides
    @Singleton
    @DatabaseName
    fun provideDatabaseName(): String = "test-architecture-database"
}