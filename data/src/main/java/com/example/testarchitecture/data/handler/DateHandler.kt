package com.example.testarchitecture.data.handler

import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DateHandler @Inject constructor() {

    fun getCurrentDate(pattern: String): String {
        return SimpleDateFormat(pattern).format(Date())
    }

    fun getDate(pattern: String, time: Long): String {
        return SimpleDateFormat(pattern).format(Date(time))
    }
}