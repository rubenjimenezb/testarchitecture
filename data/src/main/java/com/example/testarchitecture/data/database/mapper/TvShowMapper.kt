package com.example.testarchitecture.data.database.mapper

import com.example.testarchitecture.data.database.model.DbTvShow
import com.example.testarchitecture.domain.model.TvShow

fun List<DbTvShow>.toDomain(): List<TvShow> = this.map { it.toDomain() }

fun DbTvShow.toDomain(): TvShow {
    return TvShow(
        id = this.id,
        title = this.title,
        imageUrl = this.imageUrl,
        overview = this.overview,
        voteAverage = this.voteAverage,
        similarList = if (!this.similarList.isNullOrEmpty()) this.similarList.toDomain() else { emptyList()}
    )
}

fun List<TvShow>.toDatabase(): List<DbTvShow> =
    this.map { it.toDatabase() }

fun TvShow.toDatabase(): DbTvShow {
    return DbTvShow(
        id = this.id,
        title = this.title,
        imageUrl = this.imageUrl,
        overview = this.overview,
        voteAverage = this.voteAverage,
        similarList = if (!this.similarList.isNullOrEmpty()) this.similarList.toDatabase() else { emptyList()}
    )
}
