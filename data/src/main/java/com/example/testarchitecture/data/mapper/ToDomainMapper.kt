package com.example.testarchitecture.data.mapper

import com.example.testarchitecture.data.model.TvShowsResponse
import com.example.testarchitecture.data.service.MOVIE_DATABASE_IMAGE_BASE_URL
import com.example.testarchitecture.domain.model.TvShowsDomainResponse
import com.example.testarchitecture.domain.model.TvShow

fun TvShowsResponse.toDomain(): TvShowsDomainResponse {
    return TvShowsDomainResponse(
        page = this.page,
        totalPages = this.totalPages,
        results = this.results.map {
            TvShow(
                id = it.id,
                title = it.name,
                imageUrl = MOVIE_DATABASE_IMAGE_BASE_URL + it.posterPath,
                overview = it.overview,
                voteAverage = it.voteAverage,
            )
        }
    )
}