package com.example.testarchitecture.data.database.converters

import androidx.room.TypeConverter
import com.example.testarchitecture.data.database.model.DbTvShow
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class SessionConverter {
    @TypeConverter
    fun fromJson(value: String?): List<DbTvShow?>? {
        val listType: Type = object : TypeToken<List<DbTvShow?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun toJson(list: List<DbTvShow?>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}