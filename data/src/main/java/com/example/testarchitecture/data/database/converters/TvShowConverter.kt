package com.example.testarchitecture.data.database.converters

import androidx.room.TypeConverter
import com.example.testarchitecture.data.database.model.DbSession
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class TvShowConverter {
    @TypeConverter
    fun fromJson(value: String?): List<DbSession?>? {
        val listType: Type = object : TypeToken<List<DbSession?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun toJson(list: List<DbSession?>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}