package com.example.testarchitecture.data.database.datasource

import com.example.testarchitecture.data.database.dao.SessionDao
import com.example.testarchitecture.data.database.mapper.toDatabase
import com.example.testarchitecture.data.database.mapper.toDomain
import com.example.testarchitecture.domain.model.Session
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalSessionDatasource @Inject constructor(
    private val sessionDao: SessionDao,
) {

    fun getSession(): Session {
        sessionDao.getAll().firstOrNull()?.let { session ->
            return session.toDomain()
        } ?: kotlin.run {
            //If there is not session yet. Then create one
            val newSession = Session(
                idSession = 0,
                topRatedTvShowLastPageAsked = 0,
                topRatedTvShowTotalPages = 0,
                dateCachePetition = ""
            )
            sessionDao.insert(newSession.toDatabase())
            return newSession
        }
    }

    fun setSession(session: Session) {
        sessionDao.insert(session.toDatabase())
    }

    fun removeTvShows() {
        sessionDao.deleteAll()
    }
}