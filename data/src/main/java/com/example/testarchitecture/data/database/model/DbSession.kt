package com.example.testarchitecture.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

const val DATE_PATTERN_FOR_DATABASE = "dd/MM/yyyy HH:mm:ss"

@Entity(tableName = "Session")
data class DbSession (
    @PrimaryKey
    val idSession: Int,
    val topRatedTvShowPagesAsked: Int,
    val topRatedTvShowTotalPages: Int,
    val dateCachePetition: String,
)