package com.example.testarchitecture.data.database.mapper

import com.example.testarchitecture.data.database.model.DbSession
import com.example.testarchitecture.domain.model.Session

fun List<DbSession>.toDomain(): List<Session> = this.map { it.toDomain() }

fun DbSession.toDomain(): Session {
    return Session(
        idSession = this.idSession,
        topRatedTvShowLastPageAsked = this.topRatedTvShowPagesAsked,
        topRatedTvShowTotalPages = this.topRatedTvShowTotalPages,
        dateCachePetition = this.dateCachePetition
    )
}

fun List<Session>.toDatabase(): List<DbSession> =
    this.map { it.toDatabase() }

fun Session.toDatabase(): DbSession {
    return DbSession(
        idSession = this.idSession,
        topRatedTvShowPagesAsked = this.topRatedTvShowLastPageAsked,
        topRatedTvShowTotalPages = this.topRatedTvShowTotalPages,
        dateCachePetition = this.dateCachePetition
    )
}
