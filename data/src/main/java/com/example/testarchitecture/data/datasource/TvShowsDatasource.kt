package com.example.testarchitecture.data.datasource

import com.example.testarchitecture.data.mapper.toDomain
import com.example.testarchitecture.data.model.TvShowsResponse
import com.example.testarchitecture.data.service.MovieDatabaseService
import com.example.testarchitecture.domain.error.Failure
import com.example.testarchitecture.domain.functional.Either
import com.example.testarchitecture.domain.model.TvShowsDomainResponse
import javax.inject.Inject

class TvShowsDatasource @Inject constructor(
    private val service: MovieDatabaseService,
) : BaseNetworkDatasource() {

    fun getTopRatedTvShows(page: Int? = null): Either<Failure, TvShowsDomainResponse>{
        return request(
            service.getTopRatedTvShows(page = page),
            { response -> response.toDomain()},
            TvShowsResponse()
        )
    }

    fun getSimilarTvShows(id: Int): Either<Failure, TvShowsDomainResponse>{
        return request(
            service.getSimilarTvShows(id = id),
            { response -> response.toDomain()},
            TvShowsResponse()
        )
    }
}
