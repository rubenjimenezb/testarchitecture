package com.example.testarchitecture.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TvShow")
data class DbTvShow(
    @PrimaryKey val id: Int,
    val title: String,
    val imageUrl: String,
    val overview: String,
    val voteAverage: Float,
    val similarList: List<DbTvShow> = emptyList()

)
