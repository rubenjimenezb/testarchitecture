package com.example.testarchitecture.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.testarchitecture.data.database.model.DbTvShow

@Dao
interface TvShowDao {

    @Query("SELECT * FROM TvShow ORDER BY voteAverage DESC")
    fun getAll(): List<DbTvShow>

    @Query("SELECT * FROM TvShow WHERE id=:id")
    fun getById(id: Int): DbTvShow?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list: List<DbTvShow>)

    @Query("DELETE FROM TvShow")
    fun deleteAll()
}