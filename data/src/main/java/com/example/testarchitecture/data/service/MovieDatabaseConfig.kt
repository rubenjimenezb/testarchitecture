package com.example.testarchitecture.data.service

const val MOVIE_DATABASE_BASE_URL = "https://api.themoviedb.org/3/"
const val MOVIE_DATABASE_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500/"
const val MOVIE_DATABASE_TOP_RATED_ENDPOINT = "tv/top_rated"
const val MOVIE_DATABASE_SIMILAR_SHOWS_ENDPOINT = "tv/{tv_id}/similar"
const val MOVIE_DATABASE_API_KEY_VALUE = "api_key"
const val MOVIE_DATABASE_API_KEY_KEY = "fc70477bee9f7a2bc3422000e710376e"
const val MOVIE_DATABASE_PAGE_VALUE = "page"
const val MOVIE_DATABASE_DEFAULT_TIMEOUT = 10L

