package com.example.testarchitecture.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.testarchitecture.data.database.model.DbSession

@Dao
interface SessionDao {

    @Query("SELECT * FROM Session")
    fun getAll(): List<DbSession>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg list: DbSession)

    @Query("DELETE FROM Session")
    fun deleteAll()
}