package com.example.testarchitecture.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.testarchitecture.data.database.converters.SessionConverter
import com.example.testarchitecture.data.database.converters.TvShowConverter
import com.example.testarchitecture.data.database.dao.SessionDao
import com.example.testarchitecture.data.database.dao.TvShowDao
import com.example.testarchitecture.data.database.model.DbSession
import com.example.testarchitecture.data.database.model.DbTvShow

@Database(
    entities = [
        DbTvShow::class,
        DbSession::class,
    ],
    version = 1
)
@TypeConverters(
    value = [
        TvShowConverter::class,
        SessionConverter::class,
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun tvShowDao(): TvShowDao
    abstract fun sessionDao(): SessionDao

    companion object {
        @Volatile
        private var _instance: AppDatabase? = null

        fun getDatabase(
            context: Context,
            databaseName: String,
            debug: Boolean = false
        ): AppDatabase {
            return synchronized(this) {
                _instance ?: run {
                    val instance = if (debug) {
                        Room.inMemoryDatabaseBuilder(
                            context,
                            AppDatabase::class.java
                        ).allowMainThreadQueries()
                    } else {
                        Room.databaseBuilder(
                            context,
                            AppDatabase::class.java, databaseName
                        )
                    }
                        //.addMigrations()
                        .build()

                    _instance = instance
                    instance
                }
            }
        }
    }
}