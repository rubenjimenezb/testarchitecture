package com.example.testarchitecture.data.repository

import android.util.Log
import com.example.testarchitecture.data.database.datasource.LocalSessionDatasource
import com.example.testarchitecture.data.database.datasource.LocalTvShowDatasource
import com.example.testarchitecture.data.database.model.DATE_PATTERN_FOR_DATABASE
import com.example.testarchitecture.data.datasource.TvShowsDatasource
import com.example.testarchitecture.data.handler.DateHandler
import com.example.testarchitecture.domain.error.Failure
import com.example.testarchitecture.domain.functional.Either
import com.example.testarchitecture.domain.model.Session
import com.example.testarchitecture.domain.model.TvShowsDomainResponse
import com.example.testarchitecture.domain.model.TvShow
import com.example.testarchitecture.domain.repository.TvShowsRepository
import javax.inject.Inject

class TvShowsDataRepository @Inject constructor(
    private val tvShowsDatasource: TvShowsDatasource,
    private val localTvShowDatasource: LocalTvShowDatasource,
    private val localSessionDatasource: LocalSessionDatasource,
    private val dateHandler: DateHandler,
    ) : TvShowsRepository {

    override suspend fun getTopRatedTvShows(): Either<Failure, List<TvShow>> {
        //TODO:Rj we should control cache time to reset it one every hour or something similar.
        val localTvShows = localTvShowDatasource.getTvShows()

        return if(localTvShows.isNullOrEmpty()){
            tvShowsDatasource.getTopRatedTvShows().fold(
                ifLeft = {
                    Either.Left(it)
                },
                ifRight = { response ->
                    Log.d("TvShowsDataRepository", "TvShows retrieved from service")
                    storeResponseInCache(response)
                    Either.Right(localTvShowDatasource.getTvShows())
                }
            )
        }else{
            Log.d("TvShowsDataRepository", "TvShows retrieved from cache")
            Either.Right(localTvShowDatasource.getTvShows())
        }
    }

    override suspend fun getNextPageOfTopRatedTvShows(): Either<Failure, List<TvShow>> {
        //Get page from session
       localSessionDatasource.getSession().let { session ->
           val nextPage = session.topRatedTvShowLastPageAsked + 1
           return if(nextPage == session.topRatedTvShowTotalPages){
               //No more pages, then we return the same list that we already have
               Log.d("TvShowsDataRepository", "TvShows no more next pages")
               Either.Right(localTvShowDatasource.getTvShows())
           }else{
               tvShowsDatasource.getTopRatedTvShows(page = nextPage).fold(
                   ifLeft = {
                       Either.Left(it)
                   },
                   ifRight = { response ->
                       Log.d("TvShowsDataRepository", "TvShows next page retrieved from service")
                       updateResponseInCache(session, response)
                       Either.Right(localTvShowDatasource.getTvShows())
                   }
               )
           }
       }
    }

    override suspend fun getTvShowDetail(id: Int): Either<Failure, TvShow?> {
        return Either.Right(localTvShowDatasource.getTvShowById(id))
    }

    override suspend fun getSimilarTvShows(id: Int): Either<Failure, TvShow?> {
        return tvShowsDatasource.getSimilarTvShows(id).fold(
            ifLeft = {
                Either.Left(it)
            },
            ifRight = { response ->
                Log.d("TvShowsDataRepository", "Similar TvShows retrieved from service")
                //Once retrieved from the service, store it in the cache for that detail.
                val detailTvShow = localTvShowDatasource.getTvShowById(id = id)

                detailTvShow?.let { tvShow ->
                    tvShow.similarList = response.results
                    localTvShowDatasource.setTvShows(listOf(tvShow))
                }
                Either.Right(detailTvShow)
            }
        )
    }

    /**
     * This method stores the response in cache.
     */
    private fun storeResponseInCache(response: TvShowsDomainResponse){
        val session = Session(
            topRatedTvShowLastPageAsked = response.page,
            topRatedTvShowTotalPages = response.totalPages,
            dateCachePetition = dateHandler.getCurrentDate(DATE_PATTERN_FOR_DATABASE)
        )
        storeInCache(session, response.results)
    }

    /**
     * This method updates the response in cache.
     */
    private fun updateResponseInCache(session : Session, response: TvShowsDomainResponse){
        session.topRatedTvShowLastPageAsked = response.page
        session.topRatedTvShowTotalPages = response.totalPages
        storeInCache(session, response.results)
    }

    /**
     * This method stores in cache a session and a list.
     */
    private fun storeInCache(session: Session, tvShowsList: List<TvShow>) {
        localSessionDatasource.setSession(session)
        localTvShowDatasource.setTvShows(tvShowsList)
    }
}