package com.example.testarchitecture.data.database.datasource

import com.example.testarchitecture.data.database.dao.TvShowDao
import com.example.testarchitecture.data.database.mapper.toDatabase
import com.example.testarchitecture.data.database.mapper.toDomain
import com.example.testarchitecture.domain.error.Failure
import com.example.testarchitecture.domain.functional.Either
import com.example.testarchitecture.domain.model.TvShow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalTvShowDatasource @Inject constructor(
    private val tvShowDao: TvShowDao,
) {

    fun getTvShows(): List<TvShow> {
        val tvShowList = tvShowDao.getAll()
        return if (tvShowList.isEmpty()) {
            emptyList()
        } else {
            tvShowList.toDomain()
        }
    }

    fun getTvShowById(id: Int): TvShow? {
        return tvShowDao.getById(id)?.toDomain()

    }

    fun setTvShows(tvShowList: List<TvShow>) {
        tvShowDao.insertAll(tvShowList.toDatabase())
    }

    fun removeTvShows() {
        tvShowDao.deleteAll()
    }
}